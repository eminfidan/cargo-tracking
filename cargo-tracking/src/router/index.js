import Vue from 'vue'
import VueRouter from 'vue-router'
import HomePage from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomePage
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/admin',
    name: 'Admin',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "admin" */ '../views/Admin.vue')
  },
  {
    path: '/signin',
    name: 'Sign in',
    component: () => import(/* webpackChunkName: "signin" */ '../views/Login/Signin.vue')
  },
  {
    path: '/signup',
    name: '/Sign up',
    component: () => import(/* webpackChunkName: "signup" */ '../views/Login/Signup.vue')
  },
  {
    path: '/reset',
    name: '/Reset',
    component: () => import(/* webpackChunkName: "reset" */ '../views/Login/Reset.vue')
  },
  {
    path: '/*',
    component: (NotFound) => import(/* webpackChunkName: "404" */ '../components/404.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
